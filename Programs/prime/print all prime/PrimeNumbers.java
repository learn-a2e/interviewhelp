import java.util.Arrays;
import java.util.*;

class PrimeNumbers {

	public static void main(String args[]) {
		boolean isprime = false;
		int arr[] = new int[100];
		arr[0] = 2;
		arr[1] = 3;
		int arrnum = 2;
		for(int num = 2; num <= 100; num++){			
			for(int i = 2; i<= num/2; i++){
				if((num%i) == 0){
					isprime = false;			
				}else{
					isprime = true;								
				}			
			}	
			if(isprime){
				arr[arrnum] = num;
				System.out.println(num);
				arrnum++;	
			}
		}
		System.out.println(Arrays.toString(arr));	
	}
}
