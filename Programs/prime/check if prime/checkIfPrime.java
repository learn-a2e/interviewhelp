import java.util.Arrays;
import java.util.*;

class checkIfPrime {

	public static void main(String args[]) {
		System.out.println("Enter your number: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		boolean prime = true;
		if(num == 1)
			prime = false;
		else{
			for(int i = 2; i<= num/2; i++){
				if((num%i) == 0){
					prime = false;				
					break;
							
				}		
			}
		}
		System.out.println("Prime:"+ prime);		
	}
}
