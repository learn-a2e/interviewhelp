import java.util.Arrays;

class SortArray {

	public static void main(String args[]) {

		int initialarray[] = { 20,10, 20, 20, 9,9, 1, 1,1,3, 5, 6, 7, 8 };
		// int sortedArray[] = new int[initialarray.length] 
		int sortedArray[] = sortArray(initialarray, initialarray.length);
		System.out.println(Arrays.toString(sortedArray));


		int outputSecondHighest = getSecondHighest(sortedArray, sortedArray.length);
		System.out.println(outputSecondHighest);

		int outputSecondLowest = getSecondLowest(sortedArray, sortedArray.length);
		System.out.println(outputSecondLowest);

	}

	//Sort array in ascending order
	public static int[] sortArray(int array[], int size) {
		int temp;
		for (int pass = 1; pass < size; pass++) {
			for (int i = 0; i < size - pass; i++) {
				if (array[i] == array[i+1]){

				}
				if (array[i] > array[i + 1]) {
					temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
		}
		return array;
	}  

	//get the second higest from the array by removing duplicates
	public static int getSecondHighest(int array[], int size) {
		int Highest = array[size  -  1];
		int SecondHighest = array[size  -  2];

		for(int a = size-1 ; a > 0 ; a--){
				if(Highest != SecondHighest ){
					return SecondHighest;
				}else{
					SecondHighest = array[a-2];
				}
		}
		return SecondHighest;	
	}
  
	//get the second lowest from the array by removing duplicates
	public static int getSecondLowest(int array[], int size) {
		int Lowest = array[0];
		int SecondLowest = array[1];

		for(int a = 0 ; a < size ; a++){
				if(Lowest != SecondLowest ){
					return SecondLowest;
				}else{
					SecondLowest = array[a + 2];
				}
		}
		return SecondLowest;	
	}
}