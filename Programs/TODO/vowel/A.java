import java.util.*;

class A {

	public static void main(String args[]) {
		reverseInt(1234);
		reverseString("1234");
	  
        }
	 
	public static void reverseInt(int value){
		 int original_number = value;
                 int length = String.valueOf(original_number).length();
                 int final_number = 0;
                 int reverse=0;
      
          for (int i = length; length > 0 ; length--){
             reverse = original_number%10;	
             original_number = (int)original_number/10;
             final_number = final_number + (int)(reverse*(Math.pow(10,length-1))) ;
          }
             System.out.println("Int reversed ==> " + final_number);
	 }
	 
	public static void reverseString(String value){
		 String original_number = value;
                 int length = original_number.length();
                 String reverse="";
      
          for (int i = length; length > 0 ; length--){
             reverse = reverse + original_number.charAt(length -1);	
          }
            System.out.println("string reversed ==> " + reverse);
        }
	 
}
